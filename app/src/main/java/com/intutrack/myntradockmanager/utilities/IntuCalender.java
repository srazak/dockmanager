package com.intutrack.myntradockmanager.utilities;

public class IntuCalender /*extends LinearLayout implements DatePickerDialog.OnDateSetListener*/ {
   /* ArrayList<Date> dates = new ArrayList<>();
    RecyclerView rv_dates;
    ImageView calendar;
    CalenderAdapter adapter;
    Context context;
    CalenderAdapter.CalenderAdapterCallback calenderAdapterCallback;
    OnIntuDateSelect callback = null;
    CalenderConfig config = new CalenderConfig();
    Calendar newCalendar = Calendar.getInstance();
    DatePickerDialog datePickerDialog = new DatePickerDialog(
            context, this, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

    public IntuCalender(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init(context,attrs);
    }

    private void initComponents() {
        calendar = findViewById(R.id.iv_calender);
        rv_dates = findViewById(R.id.rv_dates);
        calenderAdapterCallback = new CalenderAdapter.CalenderAdapterCallback() {
            @Override
            public void OnDateClick(Date date) {
                if (callback != null)
                    callback.OnDateSelected(date);
            }
        };
        calendar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        });
        setDateList();
    }

    private void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.calender, this);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.IntuCalender);
        try {
            config.setBackground(typedArray.getDrawable(R.styleable.IntuCalender_background));
            config.setCardBackground(typedArray.getDrawable(R.styleable.IntuCalender_cardBackground));
            config.setDateColor(typedArray.getColor(R.styleable.IntuCalender_textColorDate, 0));
            config.setDayColor(typedArray.getColor(R.styleable.IntuCalender_textColorDay, 0));
            config.setIcon(typedArray.getDrawable(R.styleable.IntuCalender_icon));
        }catch (Exception e){
            typedArray.recycle();
        }
        typedArray.recycle();
        initComponents();
    }

    private void setDateList(){
        Date date = new Date(System.currentTimeMillis());
        for (int i = 0; i<6 ; i++){
            dates.add(incrementDateByOne(date,i));
        }
        adapter = new CalenderAdapter(context,dates, calenderAdapterCallback);

    }

    public void setOnDateSelectListener(OnIntuDateSelect listener){
        callback = listener;
    }


    public Date incrementDateByOne(Date date, int i) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, i);
        Date nextDate = c.getTime();
        return nextDate;
    }

    public Date decrementDateByOne(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, -1);
        Date previousDate = c.getTime();
        return previousDate;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

    }

    public interface OnIntuDateSelect {
        void OnDateSelected(Date date);
    }

    class CalenderConfig {
        int dayColor;
        int dateColor;
        Drawable background, cardBackground, icon;

        public CalenderConfig(int dayColor, int dateColor, Drawable background, Drawable cardBackground, Drawable icon) {
            this.dayColor = dayColor;
            this.dateColor = dateColor;
            this.background = background;
            this.cardBackground = cardBackground;
            this.icon = icon;
        }

        public CalenderConfig() {
        }

        public int getDayColor() {
            return dayColor;
        }

        public void setDayColor(int dayColor) {
            this.dayColor = dayColor;
        }

        public int getDateColor() {
            return dateColor;
        }

        public void setDateColor(int dateColor) {
            this.dateColor = dateColor;
        }

        public Drawable getBackground() {
            return background;
        }

        public void setBackground(Drawable background) {
            this.background = background;
        }

        public Drawable getCardBackground() {
            return cardBackground;
        }

        public void setCardBackground(Drawable cardBackground) {
            this.cardBackground = cardBackground;
        }

        public Drawable getIcon() {
            return icon;
        }

        public void setIcon(Drawable icon) {
            this.icon = icon;
        }
    }*/
}
