package com.intutrack.myntradockmanager.utilities;


public class AppSingleton {

    private static AppSingleton single_instance = null;

    int peekHeight = 0;
    String epodID = null;
    String credits = null;
    boolean hasCredits = false;

    boolean showFilters = false;

    public synchronized static AppSingleton getInstance() {
        if (single_instance == null)
            single_instance = new AppSingleton();

        return single_instance;
    }


    public boolean isHasCredits() {
        return hasCredits;
    }

    public void setHasCredits(boolean hasCredits) {
        this.hasCredits = hasCredits;
    }

    public String getCredits() {
        return credits;
    }

    public void setCredits(String credits) {
        this.credits = credits;
    }


    public boolean isShowFilters() {
        return showFilters;
    }

    public void setShowFilters(boolean showFilters) {
        this.showFilters = showFilters;
    }

    public void clearEpodID() {
        epodID = null;
    }

    public String getEpodID() {
        return epodID;
    }

    public void setEpodID(String epodID) {
        this.epodID = epodID;
    }

    public int getPeekHeight() {
        return peekHeight;
    }

    public void setPeekHeight(int peekHeight) {
        this.peekHeight = peekHeight;
    }

}
