package com.intutrack.myntradockmanager

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.intutrack.myntradockmanager.retrofit.helper.APIUtility
import com.intutrack.myntradockmanager.bean.BaseResponse
import com.intutrack.myntradockmanager.bean.docks.DockOutRequest
import com.intutrack.myntradockmanager.bean.docks.DockResponse
import com.intutrack.myntradockmanager.bean.docks.DocksResult
import com.intutrack.myntradockmanager.bean.openTransactions.ContainerId
import com.intutrack.myntradockmanager.bean.openTransactions.OpenTransactionResult
import com.intutrack.myntradockmanager.bean.openTransactions.OpenTransactionsResponse
import com.intutrack.myntradockmanager.bean.updateTrip.UpdateLoadingEnd
import com.intutrack.myntradockmanager.bean.updateTrip.UpdateLoadingStart
import com.intutrack.myntradockmanager.ui.*
import com.intutrack.myntradockmanager.ui.adapters.DockAdapter
import com.intutrack.myntradockmanager.ui.adapters.DockOutAdapter
import com.intutrack.myntradockmanager.ui.adapters.LoadingAdapter
import com.intutrack.myntradockmanager.ui.adapters.OpenTransactionAdapter
import com.intutrack.myntradockmanager.utilities.CommonUtils
import com.intutrack.myntradockmanager.utilities.Constants

class MainActivity : BaseActivity(), DockAdapter.DockAdapterCallbacks, OpenTransactionAdapter.OpenTransactionAdapterCallback, TextWatcher, CompoundButton.OnCheckedChangeListener,
    LoadingAdapter.LoadingAdapterCallbacks {
    private var tabLayout: TabLayout? = null
    private var tabSelection = 0
    lateinit var rv_home: RecyclerView
    lateinit var fab: FloatingActionButton
    lateinit var fab_logout: FloatingActionButton
    lateinit var et_search: EditText
    lateinit var dockResultData: DocksResult


    private val ar_incoming: ArrayList<OpenTransactionResult> = ArrayList()
    private val ar_outgoing: ArrayList<OpenTransactionResult> = ArrayList()
    private var sw_incoming: SwitchCompat? = null
    private var ll_sw: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    fun init() {
        tabLayout = findViewById(R.id.tabs)
        et_search = findViewById(R.id.et_search_home)
        rv_home = findViewById(R.id.rv_home)
        rv_home.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)


        sw_incoming = findViewById(R.id.sw_incoming)
        sw_incoming!!.setOnCheckedChangeListener(this)
        ll_sw = findViewById(R.id.ll_sw)

        fab = findViewById(R.id.fab)
        fab_logout = findViewById(R.id.fab_logout)
        fab_logout.setOnClickListener {
            logout()
        }
        fab.setOnClickListener {
            showNewTripDialog()
        }

        et_search.addTextChangedListener(this)

        setupTabs()
    }

    override fun onResume() {
        super.onResume()
        val tab = tabLayout!!.getTabAt(tabLayout!!.selectedTabPosition)
        tab!!.select()
        if (tabLayout!!.selectedTabPosition == 0) GetOpenTransactions()
    }

    private fun setupTabs() {
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Open Transactions"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Docks"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Loading/Unloading"))

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {

                when (tab.position) {
                    0 -> {
                        ll_sw!!.visibility = View.VISIBLE
                        GetOpenTransactions()
                    }
                    1 -> {
                        ll_sw!!.visibility = View.GONE
                        GetDocks()
                    }
                    2 -> {
                        ll_sw!!.visibility = View.GONE
                        GetLoadingData()
                    }

                }
            }
        })
    }


    fun GetOpenTransactions() {
        apiUtility.GetOpenTransactions(this, true, object : APIUtility.APIResponseListener<OpenTransactionsResponse> {
            override fun onReceiveResponse(response: OpenTransactionsResponse?) {
                ar_incoming.clear()
                ar_outgoing.clear()
                rv_home.adapter = OpenTransactionAdapter(this@MainActivity, response!!.result)
                for (i in response?.result) {
                    if (i.vehicle_status.equals(
                            "incoming", true
                        )
                    ) ar_incoming.add(i) else ar_outgoing.add(i)
                }
                rv_home.adapter = OpenTransactionAdapter(
                    this@MainActivity, if (sw_incoming!!.isChecked) ar_incoming else ar_outgoing
                )
            }
        })
    }

    fun GetDocks() {
        apiUtility.GetDocks(this, true, object : APIUtility.APIResponseListener<DockResponse> {
            override fun onReceiveResponse(response: DockResponse?) {
                rv_home.adapter = DockAdapter(this@MainActivity, response!!.result)
            }
        })
    }

    fun GetLoadingData() {
        apiUtility.GetDocks(this, true, object : APIUtility.APIResponseListener<DockResponse> {
            override fun onReceiveResponse(response: DockResponse?) {
                val loading: ArrayList<DocksResult> = ArrayList()
                for (item in response!!.result) {
                    if (item.isOccupied) loading.add(item)
                }
                rv_home.adapter = LoadingAdapter(this@MainActivity, loading)
            }
        })
    }

    fun UpdateLoadingStart(data: DocksResult, updateLoadingStart: UpdateLoadingStart) {
        apiUtility.UpdateLoadingStart(this, true, updateLoadingStart, data.ongoing_trip_data._id, object : APIUtility.APIResponseListener<BaseResponse> {
            override fun onReceiveResponse(response: BaseResponse?) {
                GetLoadingData()
            }
        })
    }

    fun UpdateLoadingEnd(data: DocksResult, updateLoadingEnd: UpdateLoadingEnd) {
        apiUtility.UpdateLoadingEnd(this, true, updateLoadingEnd, data.ongoing_trip_data._id, object : APIUtility.APIResponseListener<BaseResponse> {
            override fun onReceiveResponse(response: BaseResponse?) {
                GetLoadingData()
            }

        })
    }

    override fun AssignDock(data: OpenTransactionResult) {
        val intent = Intent(this, AssignDockActivity::class.java)
        intent.putExtra("data", data)
        startActivity(intent)
    }

    override fun OnDockIn(data: DocksResult) {
        if (data.tripList.size > 0) {
            val intent = Intent(this, AssignVehicleActivity::class.java)
            intent.putExtra("data", data.tripList)
            intent.putExtra("dockId", data._id)
            startActivity(intent)
        } else {
            CommonUtils.alert(this, "No Available Transaction on this Dock")
        }

    }

    override fun OnDockOut(data: DocksResult) {
        CommonUtils.log("DATA", Gson().toJson(data))
        //showContainerDialog(data)
        DockOut(null, data.ongoing_trip_data._id, data._id, DockOutRequest())
    }

    fun DockOut(dialog: Dialog?, tripId: String, dockId: String, dockrequest: DockOutRequest) {
        apiUtility.DockOut(this, true, dockrequest, dockId, tripId, object : APIUtility.APIResponseListener<BaseResponse> {
            override fun onReceiveResponse(response: BaseResponse?) {
                CommonUtils.alert(this@MainActivity, response!!.message)
                dialog?.dismiss()
                GetDocks()
            }
        })
    }


    override fun OnLoadingClick(data: DocksResult) {
        UpdateLoadingStart(data, UpdateLoadingStart(System.currentTimeMillis()))
    }

    override fun OnUnloadingClick(data: DocksResult) {
        dockResultData = data
        startActivityForResult(
            Intent(this, DocumentsActivity::class.java).apply {
                putExtra("data", data.ongoing_trip_data)
            }, Constants.REQUEST_DOC_UPLOAD
        )

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_CANCELED) {
            return
        }
        if (requestCode == Constants.REQUEST_DOC_UPLOAD) {
            if (resultCode == Constants.RESULT_DOC_UPDATE_COMPLETE) {
                Log.e(TAG, "onActivityResult: Activity  Result ")
                UpdateLoadingEnd(dockResultData, UpdateLoadingEnd(System.currentTimeMillis(), "", ""))
            }
        }
    }

    fun showContainerDialog(data: DocksResult) {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_container)

        val cancel: Button = dialog.findViewById(R.id.btn_cancel)
        val dock_out: Button = dialog.findViewById(R.id.btn_dock_out)
        val rv_container: RecyclerView = dialog.findViewById(R.id.rv_containers)

        rv_container.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_container.adapter = DockOutAdapter(this, data.ongoing_trip_data.container_ids)
        rv_container.setItemViewCacheSize(data.ongoing_trip_data.container_ids.size)
        cancel.setOnClickListener {
            dialog.dismiss()
        }
        dock_out.setOnClickListener {
            val adapter = rv_container.adapter as DockOutAdapter
            val request = DockOutRequest()
            request.container_ids = adapter.getListData()
            if (shouldProceed(adapter.getListData())) DockOut(dialog, data.ongoing_trip_data._id, data._id, request)
            else CommonUtils.showToast(applicationContext, "Fill all Container Ids")
        }
        dialog.setCancelable(true)
        dialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()
    }

    fun showNewTripDialog() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_add_trip)

        var radioGroup: RadioGroup = dialog.findViewById(R.id.rg_new_trip)
        val rb_dock: RadioButton = dialog.findViewById(R.id.rb_dock)
        val rb_trip: RadioButton = dialog.findViewById(R.id.rb_manual_trip)

        val cancel: Button = dialog.findViewById(R.id.btn_cancel)
        val btn_continue: Button = dialog.findViewById(R.id.btn_continue)

        btn_continue.setOnClickListener {
            if (rb_dock.isChecked) {
                dialog.dismiss()
                showNewDockDialog()
            } else if (rb_trip.isChecked) {
                val intent = Intent(this, TripActivity::class.java)
                startActivity(intent)
            }

        }

        cancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.setCancelable(true)
        dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()
    }

    private fun showNewDockDialog() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_new_dock)

        val cancel: Button = dialog.findViewById(R.id.btn_cancel)
        val create: Button = dialog.findViewById(R.id.btn_create)
        val et_dock: EditText = dialog.findViewById(R.id.et_dock_id)
        cancel.setOnClickListener {
            dialog.dismiss()
        }

        create.setOnClickListener {
            if (TextUtils.isEmpty(et_dock.text)) {
                CommonUtils.showToast(applicationContext, "Empty Field")
            } else {
                AddDock(et_dock.text.toString(), dialog)
            }
        }
        dialog.setCancelable(true)
        dialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()
    }

    private fun showUnloadingDialog(data: DocksResult) {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_unloading)

        val cancel: Button = dialog.findViewById(R.id.btn_cancel)
        val create: Button = dialog.findViewById(R.id.btn_submit)
        val et_boxes: EditText = dialog.findViewById(R.id.et_no_boxes)
        val et_other: EditText = dialog.findViewById(R.id.et_other)
        cancel.setOnClickListener {
            dialog.dismiss()
        }

        create.setOnClickListener {
            dialog.dismiss()
            UpdateLoadingEnd(data, UpdateLoadingEnd(System.currentTimeMillis(), et_boxes.text.toString(), et_other.text.toString()))
        }
        dialog.setCancelable(true)
        dialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()

    }


    private fun AddDock(dock: String, dialog: Dialog) {
        apiUtility.AddDock(this, true, dock, object : APIUtility.APIResponseListener<BaseResponse> {
            override fun onReceiveResponse(response: BaseResponse?) {
                CommonUtils.showToast(applicationContext, response!!.message)
                dialog.dismiss()
            }
        })
    }


    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        if (rv_home.adapter is DockAdapter) {
            val adapter = rv_home.adapter as DockAdapter
            adapter.filter.filter(s)
        } else if (rv_home.adapter is OpenTransactionAdapter) {
            val adapter = rv_home.adapter as OpenTransactionAdapter
            adapter.filter.filter(s)
        }
    }

    private fun shouldProceed(data: ArrayList<ContainerId>): Boolean {
        if (data.size == 0) return true
        for (pos in data.indices) {
            if (data[pos].container_id == null) return false
        }

        return true
    }

    companion object {
        private const val TAG = "MainActivity"
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        rv_home.adapter = OpenTransactionAdapter(this@MainActivity, if (isChecked) ar_incoming else ar_outgoing)
    }

}