package com.intutrack.myntradockmanager.retrofit


import com.intutrack.myntradockmanager.bean.addTrip.TripRequest
import com.intutrack.myntradockmanager.bean.BaseResponse
import com.intutrack.myntradockmanager.bean.base64ToUrl.Base64ToUrlRequest
import com.intutrack.myntradockmanager.bean.base64ToUrl.Base64ToUrlResponse
import com.intutrack.myntradockmanager.bean.docks.CreateDockRequest
import com.intutrack.myntradockmanager.bean.docks.DockOutRequest
import com.intutrack.myntradockmanager.bean.docks.DockResponse
import com.intutrack.myntradockmanager.bean.dropDown.DropDownResponse
import com.intutrack.myntradockmanager.bean.login.LoginResponse
import com.intutrack.myntradockmanager.bean.openTransactions.OpenTransactionsResponse
import com.intutrack.myntradockmanager.bean.openTransactions.UpdateDocuments
import com.intutrack.myntradockmanager.bean.updateTrip.UpdateLoadingEnd
import com.intutrack.myntradockmanager.bean.updateTrip.UpdateLoadingStart
import retrofit2.Call
import retrofit2.http.*

interface APIService {

    @GET("login")
    fun Login(): Call<LoginResponse>

    @POST("v2/trips/submit/new")
    fun AddTrip(
        @Body tripRequest: TripRequest,
        @Query("warehouse") warehouse: String
    ): Call<BaseResponse>

    @PUT("v2/trips/beta")
    fun UpdateLoadingStart(
        @Body updateLoadingStart: UpdateLoadingStart,
        @Query("tripId") tripId: String
    ): Call<BaseResponse>

    @PUT("v2/trips/beta")
    fun UpdateLoadingEnd(
        @Body updateLoadingEnd: UpdateLoadingEnd,
        @Query("tripId") tripId: String
    ): Call<BaseResponse>

    @GET("v2/docks/unassigned/trips")
    fun GetOpenTransactions(
        @Query("warehouse") warehouse: String
    ): Call<OpenTransactionsResponse>

    @GET("v2/docks")
    fun GetDocks(
        @Query("warehouse") warehouse: String
    ): Call<DockResponse>

    @POST("v2/docks/{dock_id}/dockout")
    fun DockOut(
        @Path("dock_id") dock_id: String,
        @Query("tripId") tripId: String,
        @Body tripRequest: DockOutRequest
    ): Call<BaseResponse>

    @POST("v2/docks/{dock_id}/dockin")
    fun DockIn(
        @Path("dock_id") dock_id: String,
        @Query("tripId") tripId: String,
        @Body tripRequest: TripRequest
    ): Call<BaseResponse>

    @PATCH("v2/docks/{tripId}/assign/{dockid}")
    fun AssignDock(
        @Path("dockid") dock_id: String,
        @Path("tripId") trip_id: String,
        @Query("dock") dock: String,
        @Query("warehouse") warehouse: String
    ): Call<BaseResponse>

    @POST("v2/docks/new")
    fun CreateDock(
        @Body request: CreateDockRequest,
        @Query("warehouse") warehouse: String
    ): Call<BaseResponse>

    @GET("v2/trips/dropdown/location")
    fun GetDropDown(
        @Query("locations") locations: String
    ): Call<DropDownResponse>

    @POST("v2/utils/base64-to-s3")
    fun Base64ToUrl(
        @Body request: Base64ToUrlRequest
    ): Call<Base64ToUrlResponse>

    @PUT("v2/trips/beta")
    fun UpdateDocuments(
        @Body updateDocuments: UpdateDocuments,
        @Query("tripId") tripId: String
    ): Call<BaseResponse>
}
