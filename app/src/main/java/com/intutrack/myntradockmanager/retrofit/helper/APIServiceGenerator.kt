package com.intutrack.myntradockmanager.retrofit.helper

import android.text.TextUtils
import com.google.gson.GsonBuilder
import okhttp3.Credentials
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

object APIServiceGenerator {

    val API_BASE_URL = "https://inplant.api.intutrack.com/api/"

    //val API_BASE_URL = "https://172775f54424.ngrok.io/api/"
    private var BASE_URL: String? = null
    val headers = HashMap<String, String>()
    private val httpClient = OkHttpClient.Builder()
    private val gson = GsonBuilder().enableComplexMapKeySerialization().serializeNulls().setPrettyPrinting().setLenient().setVersion(1.0).create()
    private val builder = Retrofit.Builder()
        .baseUrl(API_BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
    private var retrofit = builder.build()

    fun addHeader(key: String, value: String) {
        headers[key] = value
    }

    fun setBaseUrl(url: String) {
        BASE_URL = url
    }

    fun <S> createService(
        serviceClass: Class<S>, username: String, password: String
    ): S {
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
            val authToken = Credentials.basic(username, password)
            return createService(serviceClass, authToken)
        }

        return createService(serviceClass, null)
    }

    fun <S> createService(
        serviceClass: Class<S>, authToken: String?
    ): S {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        if (!TextUtils.isEmpty(authToken)) {
            val interceptor = AuthenticationInterceptor(authToken!!)

            if (!httpClient.interceptors().contains(interceptor))
                httpClient.addInterceptor(interceptor)
            if (!httpClient.interceptors().contains(httpLoggingInterceptor))
                httpClient.addInterceptor(httpLoggingInterceptor)
            httpClient.retryOnConnectionFailure(true)
            httpClient.connectTimeout(100, TimeUnit.SECONDS)
            httpClient.readTimeout(100, TimeUnit.SECONDS)
            httpClient.addNetworkInterceptor { chain ->
                val request = chain.request().newBuilder()

                val headers = headers

                val iterator = headers.keys.iterator()

                while (iterator.hasNext()) {
                    val key = iterator.next()
                    val value = headers[key]
                    request.addHeader(key, value)
                }

                request.method(chain.request().method(), chain.request().body())
                chain.proceed(request.build())
            }

            builder.client(httpClient.build())
            //if (retrofit == null)
            retrofit = builder.build()


        }
        return retrofit.create(serviceClass)
    }
}