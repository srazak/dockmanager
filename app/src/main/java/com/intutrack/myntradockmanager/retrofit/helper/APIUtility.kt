package com.intutrack.myntradockmanager.retrofit.helper

import android.content.Context
import android.content.Intent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.intutrack.myntradockmanager.retrofit.APIService
import com.intutrack.myntradockmanager.bean.addTrip.TripRequest
import com.intutrack.myntradockmanager.bean.BaseResponse
import com.intutrack.myntradockmanager.bean.docks.CreateDockRequest
import com.intutrack.myntradockmanager.bean.docks.DockOutRequest
import com.intutrack.myntradockmanager.bean.docks.DockResponse
import com.intutrack.myntradockmanager.bean.dropDown.DropDownResponse
import com.intutrack.myntradockmanager.bean.login.LoginResponse
import com.intutrack.myntradockmanager.bean.openTransactions.OpenTransactionsResponse
import com.intutrack.myntradockmanager.bean.updateTrip.UpdateLoadingEnd
import com.intutrack.myntradockmanager.bean.updateTrip.UpdateLoadingStart
import com.intutrack.myntradockmanager.preference.PrefEntities
import com.intutrack.myntradockmanager.preference.Preferences
import com.intutrack.myntradockmanager.R
import com.intutrack.myntradockmanager.bean.base64ToUrl.Base64ToUrlRequest
import com.intutrack.myntradockmanager.bean.base64ToUrl.Base64ToUrlResponse
import com.intutrack.myntradockmanager.bean.openTransactions.UpdateDocuments
import com.intutrack.myntradockmanager.utilities.CommonUtils
import com.intutrack.myntradockmanager.utilities.Constants
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class APIUtility(context: Context) {

    init {
        APIServiceGenerator.addHeader("Content-Type", "application/json")
        mApiService = APIServiceGenerator.createService(APIService::class.java, "Bearer " + Preferences.getPreference(context, PrefEntities.AUTH_TOKEN))

    }

    private fun showDialog(context: Context, isDialog: Boolean) {
        if (isDialog) {
            ProcessDialog.start(context)
        }
    }

    fun dismissDialog(isDialog: Boolean) {
        if (isDialog) {
            ProcessDialog.dismiss()
        }
    }

    fun Login(
        context: Context,
        isDialog: Boolean,
        username: String,
        password: String,
        listener: APIResponseListener<LoginResponse>
    ) {
        if (!CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.alert(context, context.getString(R.string.network))
            return
        }
        showDialog(context, isDialog)
        mApiService = APIServiceGenerator.createService(APIService::class.java, username, password)

        mApiService!!.Login().enqueue(object : Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                dismissDialog(isDialog)
                if (response.isSuccessful) {
                    if (response.body()!!.isStatus) {
                        listener.onReceiveResponse(response.body())
                        Preferences.setPreference(context, PrefEntities.AUTH_TOKEN, response.body()!!.result[0].token)
                        mApiService = APIServiceGenerator.createService(APIService::class.java, "Bearer " + Preferences.getPreference(context, PrefEntities.AUTH_TOKEN))
                    } else {
                        CommonUtils.alert(context, response.body()!!.message)
                    }
                } else {
                    when (response.code()) {
                        402 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_CREDIT))
                        else -> try {
                            displayErrorMessage(response.errorBody()!!.string(), context)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                dismissDialog(isDialog)
                CommonUtils.alert(context, context.getString(R.string.error))
                CommonUtils.log(TAG, t.message)
            }
        })
    }

    fun GetOpenTransactions(
        context: Context,
        isDialog: Boolean,
        listener: APIResponseListener<OpenTransactionsResponse>
    ) {
        if (!CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.alert(context, context.getString(R.string.network))
            return
        }
        showDialog(context, isDialog)
        mApiService!!.GetOpenTransactions(Preferences.getPreference(context, PrefEntities.WAREHOUSE)).enqueue(object : Callback<OpenTransactionsResponse> {

            override fun onResponse(call: Call<OpenTransactionsResponse>, response: Response<OpenTransactionsResponse>) {
                dismissDialog(isDialog)
                if (response.isSuccessful) {
                    if (response.body()!!.isStatus) {
                        listener.onReceiveResponse(response.body())
                    } else {
                        CommonUtils.alert(context, response.body()!!.message)
                    }
                } else {
                    when (response.code()) {
                        403 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_LOGOUT))
                        402 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_CREDIT))
                        else -> try {
                            displayErrorMessage(response.errorBody()!!.string(), context)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<OpenTransactionsResponse>, t: Throwable) {
                dismissDialog(isDialog)
                CommonUtils.alert(context, context.getString(R.string.error))
                CommonUtils.log(TAG, t.message)
            }
        })
    }

    fun GetDocks(
        context: Context,
        isDialog: Boolean,
        listener: APIResponseListener<DockResponse>
    ) {
        if (!CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.alert(context, context.getString(R.string.network))
            return
        }
        showDialog(context, isDialog)
        mApiService!!.GetDocks(Preferences.getPreference(context, PrefEntities.WAREHOUSE)).enqueue(object : Callback<DockResponse> {

            override fun onResponse(
                call: Call<DockResponse>, response: Response<DockResponse>
            ) {
                dismissDialog(isDialog)
                if (response.isSuccessful) {
                    if (response.body()!!.isStatus) {
                        listener.onReceiveResponse(response.body())
                    } else {
                        CommonUtils.alert(context, response.body()!!.message)
                    }
                } else {
                    when (response.code()) {
                        403 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_LOGOUT))
                        402 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_CREDIT))
                        else -> try {
                            displayErrorMessage(response.errorBody()!!.string(), context)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<DockResponse>, t: Throwable) {
                dismissDialog(isDialog)
                CommonUtils.alert(context, context.getString(R.string.error))
                CommonUtils.log(TAG, t.message)
            }
        })
    }

    fun Addtrip(
        context: Context,
        isDialog: Boolean,
        request: TripRequest,
        listener: APIResponseListener<BaseResponse>
    ) {
        if (!CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.alert(context, context.getString(R.string.network))
            return
        }
        showDialog(context, isDialog)
        mApiService!!.AddTrip(request, Preferences.getPreference(context, PrefEntities.WAREHOUSE)).enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                dismissDialog(isDialog)
                if (response.isSuccessful) {
                    if (response.body()!!.isStatus) {
                        listener.onReceiveResponse(response.body())
                    } else {
                        CommonUtils.alert(context, response.body()!!.message)
                    }
                } else {
                    when (response.code()) {
                        403 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_LOGOUT))
                        402 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_CREDIT))
                        else -> try {
                            displayErrorMessage(response.errorBody()!!.string(), context)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                dismissDialog(isDialog)
                CommonUtils.alert(context, context.getString(R.string.error))
                CommonUtils.log(TAG, t.message)
            }
        })
    }


    fun DockOut(
        context: Context,
        isDialog: Boolean,
        request: DockOutRequest,
        dockId: String,
        tripId: String,
        listener: APIResponseListener<BaseResponse>
    ) {
        if (!CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.alert(context, context.getString(R.string.network))
            return
        }
        showDialog(context, isDialog)
        mApiService!!.DockOut(dockId, tripId, request).enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                dismissDialog(isDialog)
                if (response.isSuccessful) {
                    if (response.body()!!.isStatus) {
                        listener.onReceiveResponse(response.body())
                    } else {
                        CommonUtils.alert(context, response.body()!!.message)
                    }
                } else {
                    when (response.code()) {
                        403 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_LOGOUT))
                        402 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_CREDIT))
                        else -> try {
                            displayErrorMessage(response.errorBody()!!.string(), context)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                dismissDialog(isDialog)
                CommonUtils.alert(context, context.getString(R.string.error))
                CommonUtils.log(TAG, t.message)
            }
        })
    }

    fun DockIn(
        context: Context,
        isDialog: Boolean,
        dockId: String,
        tripId: String,
        listener: APIResponseListener<BaseResponse>
    ) {
        if (!CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.alert(context, context.getString(R.string.network))
            return
        }
        showDialog(context, isDialog)
        mApiService!!.DockIn(dockId, tripId, TripRequest()).enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                dismissDialog(isDialog)
                if (response.isSuccessful) {
                    if (response.body()!!.isStatus) {
                        listener.onReceiveResponse(response.body())
                    } else {
                        CommonUtils.alert(context, response.body()!!.message)
                    }
                } else {
                    when (response.code()) {
                        403 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_LOGOUT))
                        402 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_CREDIT))
                        else -> try {
                            displayErrorMessage(response.errorBody()!!.string(), context)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                dismissDialog(isDialog)
                CommonUtils.alert(context, context.getString(R.string.error))
                CommonUtils.log(TAG, t.message)
            }
        })
    }

    fun AssignDock(
        context: Context,
        isDialog: Boolean,
        dockId: String,
        tripId: String,
        dock: String,
        listener: APIResponseListener<BaseResponse>
    ) {
        if (!CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.alert(context, context.getString(R.string.network))
            return
        }
        showDialog(context, isDialog)
        mApiService!!.AssignDock(dockId, tripId, dock, Preferences.getPreference(context, PrefEntities.WAREHOUSE)).enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                dismissDialog(isDialog)
                if (response.isSuccessful) {
                    if (response.body()!!.isStatus) {
                        listener.onReceiveResponse(response.body())
                    } else {
                        CommonUtils.alert(context, response.body()!!.message)
                    }
                } else {
                    when (response.code()) {
                        403 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_LOGOUT))
                        402 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_CREDIT))
                        else -> try {
                            displayErrorMessage(response.errorBody()!!.string(), context)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                dismissDialog(isDialog)
                CommonUtils.alert(context, context.getString(R.string.error))
                CommonUtils.log(TAG, t.message)
            }
        })
    }

    fun AddDock(
        context: Context,
        isDialog: Boolean,
        dock: String,
        listener: APIResponseListener<BaseResponse>
    ) {
        if (!CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.alert(context, context.getString(R.string.network))
            return
        }
        showDialog(context, isDialog)
        mApiService!!.CreateDock(CreateDockRequest(dock), Preferences.getPreference(context, PrefEntities.WAREHOUSE)).enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                dismissDialog(isDialog)
                if (response.isSuccessful) {
                    if (response.body()!!.isStatus) {
                        listener.onReceiveResponse(response.body())
                    } else {
                        CommonUtils.alert(context, response.body()!!.message)
                    }
                } else {
                    when (response.code()) {
                        403 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_LOGOUT))
                        402 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_CREDIT))
                        else -> try {
                            displayErrorMessage(response.errorBody()!!.string(), context)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                dismissDialog(isDialog)
                CommonUtils.alert(context, context.getString(R.string.error))
                CommonUtils.log(TAG, t.message)
            }
        })
    }

    fun GetDropDown(
        context: Context,
        isDialog: Boolean,
        locations: String,
        listener: APIResponseListener<DropDownResponse>
    ) {
        if (!CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.alert(context, context.getString(R.string.network))
            return
        }
        showDialog(context, isDialog)
        mApiService!!.GetDropDown(locations).enqueue(object : Callback<DropDownResponse> {
            override fun onResponse(
                call: Call<DropDownResponse>, response: Response<DropDownResponse>
            ) {
                dismissDialog(isDialog)
                if (response.isSuccessful) {
                    if (response.body()!!.isStatus) {
                        listener.onReceiveResponse(response.body())
                    } else {
                        CommonUtils.alert(context, response.body()!!.message)
                    }
                } else {
                    when (response.code()) {
                        403 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_LOGOUT))
                        402 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_CREDIT))
                        else -> try {
                            displayErrorMessage(response.errorBody()!!.string(), context)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<DropDownResponse>, t: Throwable) {
                dismissDialog(isDialog)
                CommonUtils.alert(context, context.getString(R.string.error))
                CommonUtils.log(TAG, t.message)
            }
        })

    }

    fun UpdateLoadingStart(
        context: Context,
        isDialog: Boolean,
        updateLoadingStart: UpdateLoadingStart,
        tripId: String,
        listener: APIResponseListener<BaseResponse>
    ){
        if (!CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.alert(context, context.getString(R.string.network))
            return
        }
        showDialog(context, isDialog)
        mApiService!!.UpdateLoadingStart(updateLoadingStart, tripId).enqueue(object : Callback<BaseResponse>{
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                dismissDialog(isDialog)
                if (response.isSuccessful) {
                    if (response.body()!!.isStatus) {
                        listener.onReceiveResponse(response.body())
                    } else {
                        CommonUtils.alert(context, response.body()!!.message)
                    }
                } else {
                    when (response.code()) {
                        403 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_LOGOUT))
                        402 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_CREDIT))
                        else -> try {
                            displayErrorMessage(response.errorBody()!!.string(), context)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                dismissDialog(isDialog)
                CommonUtils.alert(context, context.getString(R.string.error))
                CommonUtils.log(TAG, t.message)
            }

        })

    }

    fun UpdateLoadingEnd(
        context: Context,
        isDialog: Boolean,
        updateLoadingEnd: UpdateLoadingEnd,
        tripId: String,
        listener: APIResponseListener<BaseResponse>
    ) {
        if (!CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.alert(context, context.getString(R.string.network))
            return
        }
        showDialog(context, isDialog)
        mApiService!!.UpdateLoadingEnd(updateLoadingEnd, tripId).enqueue(object : Callback<BaseResponse>{
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                dismissDialog(isDialog)
                if (response.isSuccessful) {
                    if (response.body()!!.isStatus) {
                        listener.onReceiveResponse(response.body())
                    } else {
                        CommonUtils.alert(context, response.body()!!.message)
                    }
                } else {
                    when (response.code()) {
                        403 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_LOGOUT))
                        402 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_CREDIT))
                        else -> try {
                            displayErrorMessage(response.errorBody()!!.string(), context)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                dismissDialog(isDialog)
                CommonUtils.alert(context, context.getString(R.string.error))
                CommonUtils.log(TAG, t.message)
            }

        })
    }

    private fun displayErrorMessage(errorBody: String?, context: Context) {
        try {
            if (errorBody != null) {
                val `object` = JSONObject(errorBody)
                CommonUtils.alert(context, `object`.getString("message"))
            } else {
                CommonUtils.alert(context, context.getString(R.string.error))
            }
        } catch (e: JSONException) {
            e.printStackTrace()
            CommonUtils.alert(context, context.getString(R.string.error))
        }
    }

    interface APIResponseListener<T> {
        fun onReceiveResponse(response: T?)
        /* void onNetworkFailed();*/
    }

    fun Base64Tourl(
        context: Context,
        isDialog: Boolean,
        request: Base64ToUrlRequest,
        listener: APIResponseListener<Base64ToUrlResponse>
    ){
        if (!CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.alert(context, context.getString(R.string.network))
            return
        }
        showDialog(context, isDialog)
        mApiService!!.Base64ToUrl(request).enqueue(object : Callback<Base64ToUrlResponse>{
            override fun onResponse(call: Call<Base64ToUrlResponse>, response: Response<Base64ToUrlResponse>) {
                dismissDialog(isDialog)
                if (response.isSuccessful) {
                    if (response.body()!!.isStatus) {
                        listener.onReceiveResponse(response.body())
                    } else {
                        CommonUtils.alert(context, response.body()!!.message)
                    }
                } else {
                    when (response.code()) {
                        403 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_LOGOUT))
                        402 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_CREDIT))
                        else -> try {
                            displayErrorMessage(response.errorBody()!!.string(), context)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<Base64ToUrlResponse>, t: Throwable) {
                dismissDialog(isDialog)
                CommonUtils.alert(context, context.getString(R.string.error))
                CommonUtils.log(TAG, t.message)
            }

        })
    }

    fun UpdateDocuments(context: Context, isDialog: Boolean, request: UpdateDocuments, tripId : String, listener: APIResponseListener<BaseResponse>){
        if (!CommonUtils.isNetworkAvailable(context)) {
            CommonUtils.alert(context, context.getString(R.string.network))
            return
        }
        showDialog(context, isDialog)
        mApiService!!.UpdateDocuments(request, tripId).enqueue(object : Callback<BaseResponse>{
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                dismissDialog(isDialog)
                if (response.isSuccessful) {
                    if (response.body()!!.isStatus) {
                        listener.onReceiveResponse(response.body())
                    } else {
                        CommonUtils.alert(context, response.body()!!.message)
                    }
                } else {
                    when (response.code()) {
                        403 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_LOGOUT))
                        402 -> LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(Constants.ACTION_CREDIT))
                        else -> try {
                            displayErrorMessage(response.errorBody()!!.string(), context)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                dismissDialog(isDialog)
                CommonUtils.alert(context, context.getString(R.string.error))
                CommonUtils.log(TAG, t.message)
            }

        })
    }

    companion object {
        private const val TAG = "APIUtility"
        private var mApiService: APIService? = null
    }

}