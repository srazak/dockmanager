package com.intutrack.myntradockmanager.bean.docks;

import com.google.gson.annotations.SerializedName;
import com.intutrack.myntradockmanager.bean.openTransactions.ContainerId;

import java.util.ArrayList;

public class DockOutRequest {

    @SerializedName("container_ids")
    ArrayList<ContainerId> container_ids = new ArrayList<>();

    public ArrayList<ContainerId> getContainer_ids() {
        return container_ids;
    }

    public void setContainer_ids(ArrayList<ContainerId> container_ids) {
        this.container_ids = container_ids;
    }
}
