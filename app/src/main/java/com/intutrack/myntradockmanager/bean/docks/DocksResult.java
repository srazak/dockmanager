package com.intutrack.myntradockmanager.bean.docks;

import com.google.gson.annotations.SerializedName;
import com.intutrack.myntradockmanager.bean.addTrip.Driver;
import com.intutrack.myntradockmanager.bean.openTransactions.ContainerId;
import com.intutrack.myntradockmanager.bean.openTransactions.DocumentsConfig;
import com.intutrack.myntradockmanager.bean.openTransactions.OpenTransactionResult;

import java.io.Serializable;
import java.util.ArrayList;

public class DocksResult implements Serializable {

    @SerializedName("_id")
    String _id;

    @SerializedName("type")
    String type;

    @SerializedName("place")
    String place;

    @SerializedName("client")
    String client;

    @SerializedName("warehouse")
    String warehouse;

    @SerializedName("running")
    boolean running;

    @SerializedName("submitted")
    boolean submitted;

    @SerializedName("accepted")
    boolean accepted;

    @SerializedName("vehicle")
    String vehicle;
    @SerializedName("trip_type")
    String trip_type;

    @SerializedName("assigned_dock")
    String assigned_dock;

    @SerializedName("driver")
    Driver driver;

    @SerializedName("verifiedDocs")
    String verifiedDocs = "";

    @SerializedName("loadingUnloadingEnd")
    long loadingUnloadingStartEnd = 0;

    @SerializedName("loadingUnloadingStart")
    long loadingUnloadingStart = 0;

    @SerializedName("no_of_boxes")
    String no_of_boxes;

    @SerializedName("other")
    String other;

    @SerializedName("vehicle_status")
    String vehicle_status;

    @SerializedName("latestStatus")
    String latestStatus;

    @SerializedName("gate_in_time_epoch")
    long gate_in_time_epoch = 0;

    @SerializedName("start_time_epoch")
    long start_time_epoch = 0;

    @SerializedName("dock_time_epoch")
    long dock_time_epoch;

    @SerializedName("container_ids")
    ArrayList<ContainerId> container_ids = new ArrayList<>();

    @SerializedName("required_docs")
    ArrayList<DocumentsConfig> required_docs = new ArrayList<>();

    public ArrayList<DocumentsConfig> getRequired_docs() {
        return required_docs;
    }

    public void setRequired_docs(ArrayList<DocumentsConfig> required_docs) {
        this.required_docs = required_docs;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public boolean isSubmitted() {
        return submitted;
    }

    public void setSubmitted(boolean submitted) {
        this.submitted = submitted;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getTrip_type() {
        return trip_type;
    }

    public void setTrip_type(String trip_type) {
        this.trip_type = trip_type;
    }

    public String getAssigned_dock() {
        return assigned_dock;
    }

    public void setAssigned_dock(String assigned_dock) {
        this.assigned_dock = assigned_dock;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public String getVerifiedDocs() {
        return verifiedDocs;
    }

    public void setVerifiedDocs(String verifiedDocs) {
        this.verifiedDocs = verifiedDocs;
    }

    public long getLoadingUnloadingStartEnd() {
        return loadingUnloadingStartEnd;
    }

    public void setLoadingUnloadingStartEnd(long loadingUnloadingStartEnd) {
        this.loadingUnloadingStartEnd = loadingUnloadingStartEnd;
    }

    public long getLoadingUnloadingStart() {
        return loadingUnloadingStart;
    }

    public void setLoadingUnloadingStart(long loadingUnloadingStart) {
        this.loadingUnloadingStart = loadingUnloadingStart;
    }

    public String getNo_of_boxes() {
        return no_of_boxes;
    }

    public void setNo_of_boxes(String no_of_boxes) {
        this.no_of_boxes = no_of_boxes;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getVehicle_status() {
        return vehicle_status;
    }

    public void setVehicle_status(String vehicle_status) {
        this.vehicle_status = vehicle_status;
    }

    public String getLatestStatus() {
        return latestStatus;
    }

    public void setLatestStatus(String latestStatus) {
        this.latestStatus = latestStatus;
    }

    public long getGate_in_time_epoch() {
        return gate_in_time_epoch;
    }

    public void setGate_in_time_epoch(long gate_in_time_epoch) {
        this.gate_in_time_epoch = gate_in_time_epoch;
    }

    public long getStart_time_epoch() {
        return start_time_epoch;
    }

    public void setStart_time_epoch(long start_time_epoch) {
        this.start_time_epoch = start_time_epoch;
    }

    public long getDock_time_epoch() {
        return dock_time_epoch;
    }

    public void setDock_time_epoch(long dock_time_epoch) {
        this.dock_time_epoch = dock_time_epoch;
    }

    public ArrayList<ContainerId> getContainer_ids() {
        return container_ids;
    }

    public void setContainer_ids(ArrayList<ContainerId> container_ids) {
        this.container_ids = container_ids;
    }

    @SerializedName("trip_list")
    ArrayList<TripList> tripList = new ArrayList<>();

    @SerializedName("ongoing_trip_data")
    OpenTransactionResult ongoing_trip_data;

    @SerializedName("occupied")
    boolean occupied;

    boolean expanded = false;

    boolean selected = false;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public ArrayList<TripList> getTripList() {
        return tripList;
    }

    public void setTripList(ArrayList<TripList> tripList) {
        this.tripList = tripList;
    }

    public OpenTransactionResult getOngoing_trip_data() {
        return ongoing_trip_data;
    }

    public void setOngoing_trip_data(OpenTransactionResult ongoing_trip_data) {
        this.ongoing_trip_data = ongoing_trip_data;
    }

    public boolean isOccupied() {
        return occupied;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }
}
