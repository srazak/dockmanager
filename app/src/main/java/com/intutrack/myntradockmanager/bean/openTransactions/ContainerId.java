package com.intutrack.myntradockmanager.bean.openTransactions;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ContainerId implements Serializable {

    @SerializedName("lane")
    String lane;

    @SerializedName("container_id")
    String container_id;

    public String getLane() {
        return lane;
    }

    public void setLane(String lane) {
        this.lane = lane;
    }

    public String getContainer_id() {
        return container_id;
    }

    public void setContainer_id(String container_id) {
        this.container_id = container_id;
    }
}
