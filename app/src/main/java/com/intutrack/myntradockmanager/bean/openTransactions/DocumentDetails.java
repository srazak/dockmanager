package com.intutrack.myntradockmanager.bean.openTransactions;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DocumentDetails implements Serializable {

    @SerializedName("name")
    String name;

    @SerializedName("remark")
    String remark;

    @SerializedName("verified")
    boolean verified;

    @SerializedName("image")
    String image;

    public DocumentDetails(String name, String value, boolean verified) {
        this.name = name;
        this.remark = value;
        this.verified = verified;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }
}
