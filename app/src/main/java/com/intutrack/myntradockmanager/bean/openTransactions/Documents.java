package com.intutrack.myntradockmanager.bean.openTransactions;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Documents implements Serializable {

    @SerializedName("tag")
    String tag;

    @SerializedName("expanded")
    boolean expanded = false;

    @SerializedName("value")
    ArrayList<DocumentDetails> documentDetails = new ArrayList<>();

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public ArrayList<DocumentDetails> getDocumentDetails() {
        return documentDetails;
    }

    public void setDocumentDetails(ArrayList<DocumentDetails> documentDetails) {
        this.documentDetails = documentDetails;
    }
}
