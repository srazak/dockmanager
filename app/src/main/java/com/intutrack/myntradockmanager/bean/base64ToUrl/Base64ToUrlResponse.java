package com.intutrack.myntradockmanager.bean.base64ToUrl;

import com.google.gson.annotations.SerializedName;
import com.intutrack.myntradockmanager.bean.BaseResponse;

import java.util.ArrayList;

public class Base64ToUrlResponse extends BaseResponse {

    @SerializedName("result")
    ArrayList<Base64ToUrlResult> result = new ArrayList<>();

    public ArrayList<Base64ToUrlResult> getResult() {
        return result;
    }

    public void setResult(ArrayList<Base64ToUrlResult> result) {
        this.result = result;
    }
}
