package com.intutrack.myntradockmanager.bean.openTransactions;

import com.google.gson.annotations.SerializedName;
import com.intutrack.myntradockmanager.bean.BaseResponse;

import java.io.Serializable;
import java.util.ArrayList;

public class OpenTransactionsResponse extends BaseResponse implements Serializable {

    @SerializedName("result")
    ArrayList<OpenTransactionResult> result = new ArrayList<>();

    public ArrayList<OpenTransactionResult> getResult() {
        return result;
    }

    public void setResult(ArrayList<OpenTransactionResult> result) {
        this.result = result;
    }
}
