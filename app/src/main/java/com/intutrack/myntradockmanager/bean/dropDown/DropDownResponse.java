package com.intutrack.myntradockmanager.bean.dropDown;

import com.google.gson.annotations.SerializedName;
import com.intutrack.myntradockmanager.bean.BaseResponse;
import com.intutrack.myntradockmanager.bean.login.KeyValueWarehouse;

import java.util.ArrayList;

public class DropDownResponse extends BaseResponse {

    @SerializedName("result")
    ArrayList<KeyValueWarehouse> result = new ArrayList<>();

    public ArrayList<KeyValueWarehouse> getResult() {
        return result;
    }

    public void setResult(ArrayList<KeyValueWarehouse> result) {
        this.result = result;
    }
}
