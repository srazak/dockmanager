package com.intutrack.myntradockmanager.bean.updateTrip;

import com.google.gson.annotations.SerializedName;

public class UpdateLoadingEnd {

    @SerializedName("loadingUnloadingEnd")
    long loadingUnloadingStartEnd = 0;

    @SerializedName("no_of_boxes")
    String no_of_boxes;

    @SerializedName("other")
    String other;

    public UpdateLoadingEnd(long loadingUnloadingStartEnd, String no_of_boxes, String other) {
        this.loadingUnloadingStartEnd = loadingUnloadingStartEnd;
        this.no_of_boxes = no_of_boxes;
        this.other = other;
    }

    public long getLoadingUnloadingStartEnd() {
        return loadingUnloadingStartEnd;
    }

    public void setLoadingUnloadingStartEnd(long loadingUnloadingStartEnd) {
        this.loadingUnloadingStartEnd = loadingUnloadingStartEnd;
    }

    public String getNo_of_boxes() {
        return no_of_boxes;
    }

    public void setNo_of_boxes(String no_of_boxes) {
        this.no_of_boxes = no_of_boxes;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }
}
