package com.intutrack.myntradockmanager.bean.login;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ClientTripType {

    @SerializedName("client_trip_type")
    KeyValue client_trip_type;

    @SerializedName("edge_type")
    ArrayList<EdgeType> edge_type = new ArrayList<>();

    public KeyValue getClient_trip_type() {
        return client_trip_type;
    }

    public void setClient_trip_type(KeyValue client_trip_type) {
        this.client_trip_type = client_trip_type;
    }

    public ArrayList<EdgeType> getEdge_type() {
        return edge_type;
    }

    public void setEdge_type(ArrayList<EdgeType> edge_type) {
        this.edge_type = edge_type;
    }
}
