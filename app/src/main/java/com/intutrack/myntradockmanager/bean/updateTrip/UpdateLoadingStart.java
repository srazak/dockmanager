package com.intutrack.myntradockmanager.bean.updateTrip;

import com.google.gson.annotations.SerializedName;

public class UpdateLoadingStart {

    @SerializedName("loadingUnloadingStart")
    long loadingUnloadingStart = 0;

    public UpdateLoadingStart(long loadingUnloadingStart) {
        this.loadingUnloadingStart = loadingUnloadingStart;
    }

    public long getLoadingUnloadingStart() {
        return loadingUnloadingStart;
    }

    public void setLoadingUnloadingStart(long loadingUnloadingStart) {
        this.loadingUnloadingStart = loadingUnloadingStart;
    }
}
