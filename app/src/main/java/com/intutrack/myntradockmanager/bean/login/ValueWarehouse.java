package com.intutrack.myntradockmanager.bean.login;

import com.google.gson.annotations.SerializedName;

public class ValueWarehouse {

    @SerializedName("name")
    String name;

    @SerializedName("code")
    String code;

    @SerializedName("zone")
    String zone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }
}
