package com.intutrack.myntradockmanager.bean.addTrip;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Driver implements Serializable {

    @SerializedName("name")
    String name;

    @SerializedName("phone")
    String phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
