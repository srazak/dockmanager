package com.intutrack.myntradockmanager.bean.login;

import com.google.gson.annotations.SerializedName;

public class ModeData {

    @SerializedName("key")
    String key;

    @SerializedName("value")
    ValueMode valueMode;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ValueMode getValueMode() {
        return valueMode;
    }

    public void setValueMode(ValueMode valueMode) {
        this.valueMode = valueMode;
    }
}
