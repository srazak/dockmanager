package com.intutrack.myntradockmanager.bean.addTrip;

import com.google.gson.annotations.SerializedName;
import com.intutrack.myntradockmanager.bean.login.ValueMode;
import com.intutrack.myntradockmanager.bean.login.ValueWarehouse;

import java.util.ArrayList;

public class TripData {

    @SerializedName("vehicle")
    String vehicle;

    @SerializedName("driver")
    Driver driver;

    @SerializedName("vehicle_status")
    String vehicle_status;

    @SerializedName("vendor_name")
    String vendor_name;

    @SerializedName("vehicle_type")
    String vehicle_type;

    @SerializedName("drops")
    ArrayList<String> drops = new ArrayList<>();

    @SerializedName("destname")
    ValueWarehouse destname;

    @SerializedName("placement_type")
    String placement_type;

    @SerializedName("purpose")
    String purpose;

    @SerializedName("client_trip_type")
    String client_trip_type;

    @SerializedName("edge_type")
    String edge_type;

    @SerializedName("load_type")
    String load_type;

    @SerializedName("mode")
    ValueMode mode;

    public TripData(String vehicle, Driver driver, String vehicle_status, String vendor_name, String vehicle_type, ArrayList<String> drops, ValueWarehouse destname, String placement_type, String purpose) {
        this.vehicle = vehicle;
        this.driver = driver;
        this.vehicle_status = vehicle_status;
        this.vendor_name = vendor_name;
        this.vehicle_type = vehicle_type;
        this.drops = drops;
        this.destname = destname;
        this.placement_type = placement_type;
        this.purpose = purpose;
    }

    public TripData(String vehicle, Driver driver, String vehicle_status, String vendor_name, String vehicle_type,
                    ArrayList<String> drops, ValueWarehouse destname, String placement_type, String purpose, String client_trip_type, String edge_type, String load_type, ValueMode mode) {
        this.vehicle = vehicle;
        this.driver = driver;
        this.vehicle_status = vehicle_status;
        this.vendor_name = vendor_name;
        this.vehicle_type = vehicle_type;
        this.drops = drops;
        this.destname = destname;
        this.placement_type = placement_type;
        this.purpose = purpose;
        this.client_trip_type = client_trip_type;
        this.edge_type = edge_type;
        this.load_type = load_type;
        this.mode = mode;
    }

    public TripData(String vehicle, Driver driver) {
        this.vehicle = vehicle;
        this.driver = driver;
    }

    public TripData(String vehicle, Driver driver, String vehicle_status) {
        this.vehicle = vehicle;
        this.driver = driver;
        this.vehicle_status = vehicle_status;
    }

    public TripData(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getVehicle_status() {
        return vehicle_status;
    }

    public void setVehicle_status(String vehicle_status) {
        this.vehicle_status = vehicle_status;
    }

    public String getVendor_name() {
        return vendor_name;
    }

    public void setVendor_name(String vendor_name) {
        this.vendor_name = vendor_name;
    }

    public String getVehicle_type() {
        return vehicle_type;
    }

    public void setVehicle_type(String vehicle_type) {
        this.vehicle_type = vehicle_type;
    }

    public ArrayList<String> getDrops() {
        return drops;
    }

    public void setDrops(ArrayList<String> drops) {
        this.drops = drops;
    }

    public ValueWarehouse getDestname() {
        return destname;
    }

    public void setDestname(ValueWarehouse destname) {
        this.destname = destname;
    }

    public String getPlacement_type() {
        return placement_type;
    }

    public void setPlacement_type(String placement_type) {
        this.placement_type = placement_type;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public Driver getDriver() {
        return driver;
    }

  /*  public ArrayList<Manifest> getManifest() {
        return manifest;
    }

    public void setManifest(ArrayList<Manifest> manifest) {
        this.manifest = manifest;
    }*/

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }
}
