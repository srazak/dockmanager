package com.intutrack.myntradockmanager.bean.docks;

import com.google.gson.annotations.SerializedName;
import com.intutrack.myntradockmanager.bean.BaseResponse;

import java.io.Serializable;
import java.util.ArrayList;

public class DockResponse extends BaseResponse implements Serializable {

    @SerializedName("result")
    ArrayList<DocksResult> result = new ArrayList<>();

    public ArrayList<DocksResult> getResult() {
        return result;
    }

    public void setResult(ArrayList<DocksResult> result) {
        this.result = result;
    }
}
