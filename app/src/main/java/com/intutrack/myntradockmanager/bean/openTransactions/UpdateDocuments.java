package com.intutrack.myntradockmanager.bean.openTransactions;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UpdateDocuments {

    @SerializedName("required_docs")
    ArrayList<DocumentsConfig> required_docs;

    public UpdateDocuments(ArrayList<DocumentsConfig> required_docs) {
        this.required_docs = required_docs;
    }
}
