package com.intutrack.myntradockmanager.bean.login;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class EdgeType {

    @SerializedName("edge_type")
    KeyValue edge_type;

    @SerializedName("load_type")
    ArrayList<LoadType> loadType = new ArrayList<>();

    public KeyValue getEdge_type() {
        return edge_type;
    }

    public void setEdge_type(KeyValue edge_type) {
        this.edge_type = edge_type;
    }

    public ArrayList<LoadType> getLoadType() {
        return loadType;
    }

    public void setLoadType(ArrayList<LoadType> loadType) {
        this.loadType = loadType;
    }
}
