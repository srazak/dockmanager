package com.intutrack.myntradockmanager.bean.docks;

import com.google.gson.annotations.SerializedName;

public class CreateDockRequest {

    @SerializedName("name")
    String name;

    public CreateDockRequest(String name) {
        this.name = name;
    }
}
