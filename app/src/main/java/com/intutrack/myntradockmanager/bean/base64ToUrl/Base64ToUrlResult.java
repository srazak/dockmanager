package com.intutrack.myntradockmanager.bean.base64ToUrl;

import com.google.gson.annotations.SerializedName;

public class Base64ToUrlResult {

    @SerializedName("fileName")
    String fileName;

    @SerializedName("fileType")
    String fileType;

    @SerializedName("file")
    String file;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
