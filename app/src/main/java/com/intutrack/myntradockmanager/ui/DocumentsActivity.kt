package com.intutrack.myntradockmanager.ui

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Parcelable
import android.provider.MediaStore
import android.util.Base64
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.intutrack.gatekeeper.UI.Adapters.DocumentsChildAdapter
import com.intutrack.myntradockmanager.retrofit.helper.APIUtility
import com.intutrack.myntradockmanager.bean.openTransactions.DocumentDetails
import com.intutrack.myntradockmanager.bean.openTransactions.OpenTransactionResult
import com.intutrack.myntradockmanager.bean.openTransactions.UpdateDocuments
import com.intutrack.myntradockmanager.R
import com.intutrack.myntradockmanager.bean.BaseResponse
import com.intutrack.myntradockmanager.bean.base64ToUrl.Base64ToUrlRequest
import com.intutrack.myntradockmanager.bean.base64ToUrl.Base64ToUrlResponse
import com.intutrack.myntradockmanager.ui.adapters.DocumentsAdapter
import com.intutrack.myntradockmanager.utilities.CommonUtils
import com.intutrack.myntradockmanager.utilities.ImageUtils
import com.intutrack.myntradockmanager.databinding.ActivityDocumentsBinding
import com.intutrack.myntradockmanager.utilities.Constants
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

private const val REQ_CAPTURE = 100
private const val RES_IMAGE = 100

class DocumentsActivity : BaseActivity(), DocumentsChildAdapter.DocumentsChildAdapterCallback {

    private lateinit var binding: ActivityDocumentsBinding
    lateinit var data: OpenTransactionResult
    private lateinit var selectedTab: String
    private var base: String? = null
    private lateinit var documentDetails: DocumentDetails

    private var imgPath: String = ""
    private var imageUri: Uri? = null

    init {
        checkAndRequestPermissions()
    }

    private fun checkAndRequestPermissions() {
        val storagePermission = ContextCompat.checkSelfPermission(
            this, Manifest.permission.READ_EXTERNAL_STORAGE
        )
        val storageWritePermission = ContextCompat.checkSelfPermission(
            this, Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        val cameraPermission = ContextCompat.checkSelfPermission(
            this, Manifest.permission.CAMERA
        )
        val callPhone = ContextCompat.checkSelfPermission(
            this, Manifest.permission.CALL_PHONE
        )
        val listPermissionsNeeded = java.util.ArrayList<String>()

        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
        if (storageWritePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }
        if (callPhone != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CALL_PHONE)
        }

        if (listPermissionsNeeded.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                this, listPermissionsNeeded.toTypedArray(), 1233
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDocumentsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        data = intent.getSerializableExtra("data") as OpenTransactionResult
        CommonUtils.log("DocumentsActivity", Gson().toJson(data))
        binding.rvDocs.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.rvDocs.adapter = DocumentsAdapter(this, ArrayList())


        binding.tvGateIn.setOnClickListener {
            binding.tvGateIn.selectEntry()
            binding.tvGateOut.deSelectEntry()
        }

        binding.tvGateOut.setOnClickListener {
            binding.tvGateOut.selectEntry()
            binding.tvGateIn.deSelectEntry()
        }

        binding.btnUpdate.setOnClickListener {
            UpdateDocument(UpdateDocuments(data.required_docs), data._id)
        }

        binding.tvGateIn.deSelectEntry()
        binding.tvGateOut.selectEntry()
    }

    private fun TextView.selectEntry() {
        background = ContextCompat.getDrawable(this@DocumentsActivity, R.drawable.round_blue)
        setTextColor(ContextCompat.getColor(this@DocumentsActivity, R.color.textColorWhite))
        isEnabled = false
        selectedTab = text.toString()
        val adapter = binding.rvDocs.adapter as DocumentsAdapter
        for (docs in data.required_docs) {
            if (docs.event.contains(text)) adapter.setData(docs.documents)
        }
    }

    private fun TextView.deSelectEntry() {
        background = ContextCompat.getDrawable(this@DocumentsActivity, R.drawable.round_gray)
        setTextColor(ContextCompat.getColor(this@DocumentsActivity, R.color.dark_grey))
        isEnabled = true
    }


    override fun OnActionClick(data: DocumentDetails) {
        documentDetails = data

        if (data.image.isNullOrEmpty()) {
            chooseImage()
        } else {
            val intent = Intent(this, ImageActivity::class.java)
            intent.putExtra("data", data)
            startActivityForResult(intent, Constants.REQUEST_REMARK_UPDATE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        base = ""
        if (resultCode == RESULT_CANCELED) {
            return
        }
        when (requestCode) {
            RES_IMAGE -> {
                val bitmap = ImageUtils.getInstant().getCompressedBitmap(imgPath)
                val byteArrayOutputStream = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
                val byteArray1 = byteArrayOutputStream.toByteArray()
                base = Base64.encodeToString(byteArray1, Base64.NO_WRAP)
                Base64ToUrl(Base64ToUrlRequest(System.currentTimeMillis().toString(), documentDetails.name, base))
                CommonUtils.log("ALIEN", base)
            }

            Constants.REQUEST_REMARK_UPDATE -> {
                when (resultCode) {
                    Constants.RESULT_REMARK_UPDATE_COMPLETE -> {
                        val dataReceived = data!!.getSerializableExtra("data") as DocumentDetails
                        updateData("", dataReceived.remark)
                    }

                    Constants.RESULT_IMAGE_UPDATE_COMPLETE -> {
                        updateData(Constants.REMOVE_IMAGE, "")
                    }
                }
            }
        }


    }

    private fun setImageUri(): Uri {
        val folder = File("${getExternalFilesDir(Environment.DIRECTORY_DCIM)}")
        folder.mkdirs()

        val file = File(folder, "Image_Tmp.jpg")
        if (file.exists()) file.delete()
        file.createNewFile()
        imageUri = FileProvider.getUriForFile(
            this, "com.intutrack.gatekeeper.Utilities.GenericFileProvider", file
        )
        imgPath = file.absolutePath
        return imageUri!!
    }


    private fun chooseImage() {
        startActivityForResult(getPickImageIntent(), 100)
    }

    private fun getPickImageIntent(): Intent? {
        var chooserIntent: Intent? = null

        var intentList: MutableList<Intent> = ArrayList()

        val pickIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri())

        //intentList = addIntentsToList(this, intentList, pickIntent)
        intentList = addIntentsToList(this, intentList, takePhotoIntent)

        if (intentList.size > 0) {
            chooserIntent = Intent.createChooser(
                intentList.removeAt(intentList.size - 1), "Select Method"
            )
            chooserIntent!!.putExtra(
                Intent.EXTRA_INITIAL_INTENTS, intentList.toTypedArray<Parcelable>()
            )
        }

        return chooserIntent
    }

    private fun addIntentsToList(
        context: Context, list: MutableList<Intent>, intent: Intent
    ): MutableList<Intent> {
        val resInfo = context.packageManager.queryIntentActivities(intent, 0)
        for (resolveInfo in resInfo) {
            val packageName = resolveInfo.activityInfo.packageName
            val targetedIntent = Intent(intent)
            targetedIntent.setPackage(packageName)
            list.add(targetedIntent)
        }
        return list
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQ_CAPTURE -> {
                if (isAllPermissionsGranted(grantResults)) {
                    chooseImage()
                } else {
                    Toast.makeText(
                        this, "Permission not granted", Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun Base64ToUrl(request: Base64ToUrlRequest) {
        apiUtility.Base64Tourl(this, true, request, object : APIUtility.APIResponseListener<Base64ToUrlResponse> {
            override fun onReceiveResponse(response: Base64ToUrlResponse?) {
                updateData(response!!.result[0].file, "")
            }

        })
    }

    fun updateData(url: String, remark: String) {
        val adapter = binding.rvDocs.adapter as DocumentsAdapter
        val adapterData = adapter.getData()
        for (item in adapterData) {
            for (itemChild in item.documentDetails) {
                if (documentDetails.name == itemChild.name) {
                    if (!url.isNullOrEmpty()) itemChild.image = url
                    if (!remark.isNullOrEmpty()) itemChild.remark = remark
                    if (url == Constants.REMOVE_IMAGE) itemChild.image = ""
                    adapter.setData(adapterData)
                }
            }
        }
        for (item in this.data.required_docs) {
            if (item.event == selectedTab) item.documents = adapterData
        }
    }

    private fun UpdateDocument(request: UpdateDocuments, tripId: String) {
        apiUtility.UpdateDocuments(this, true, request, tripId, object : APIUtility.APIResponseListener<BaseResponse> {
            override fun onReceiveResponse(response: BaseResponse?) {
                setResult(Constants.RESULT_DOC_UPDATE_COMPLETE)
                finish()
            }
        })
    }

    companion object {
        private const val TAG = "DocumentsActivity"
        val GET_FROM_GALLERY = 3
        private val REQUEST_TAKE_PHOTO = 234
        private val KEY_BITMAP = "dsa"
    }
}