package com.intutrack.myntradockmanager.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.intutrack.myntradockmanager.bean.docks.DocksResult
import com.intutrack.myntradockmanager.R
import com.intutrack.myntradockmanager.utilities.CommonUtils

class LoadingAdapter(context: Context, val data: ArrayList<DocksResult>) : RecyclerView.Adapter<LoadingAdapter.LoadingHolder>(), Filterable {

    var list: ArrayList<DocksResult> = ArrayList()
    var mFilterList: ArrayList<DocksResult> = ArrayList()
    var callback: LoadingAdapterCallbacks
    private var valueFilter: ValueFilter? = null

    init {
        list = data
        mFilterList = data
        callback = context as LoadingAdapterCallbacks
    }

    class LoadingHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tv_veh: TextView
        var tv_dock: TextView
        var tv_verified_docs: TextView
        var tv_loading_time: TextView
        var tv_unloading_time: TextView
        var ll_loading: LinearLayout
        var ll_unloading: LinearLayout

        init {
            tv_veh = itemView.findViewById(R.id.tv_veh_no)
            tv_dock = itemView.findViewById(R.id.tv_dock)
            tv_verified_docs = itemView.findViewById(R.id.tv_verified_docs)
            tv_loading_time = itemView.findViewById(R.id.tv_loading_time)
            tv_unloading_time = itemView.findViewById(R.id.tv_unloading_time)
            ll_loading = itemView.findViewById(R.id.ll_loading)
            ll_unloading = itemView.findViewById(R.id.ll_unloading)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LoadingHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_loading, parent, false)
        return LoadingHolder(itemView)
    }

    override fun onBindViewHolder(holder: LoadingHolder, position: Int) {
        val model = list[position]
        CommonUtils.log("ALIEN", Gson().toJson(model))
        holder.tv_veh.text = model.ongoing_trip_data.vehicle
        holder.tv_dock.text = model.ongoing_trip_data.assigned_dock
        holder.tv_verified_docs.text = model.ongoing_trip_data.verifiedDocs
        holder.tv_loading_time.text = CommonUtils.convertTimestampToDate(model.ongoing_trip_data.loadingUnloadingStart).toString()
        holder.tv_unloading_time.text = CommonUtils.convertTimestampToDate(model.ongoing_trip_data.loadingUnloadingStartEnd).toString()
        holder.ll_unloading.setOnClickListener {
            callback.OnUnloadingClick(model)
        }
        holder.ll_loading.setOnClickListener {
            /*if (model.ongoing_trip_data.loadingUnloadingStart.equals(0))*/ callback.OnLoadingClick(model)
        }
        holder.ll_unloading.setOnClickListener {
            /*if (model.ongoing_trip_data.loadingUnloadingStartEnd.equals(0))*/ callback.OnUnloadingClick(model)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getFilter(): Filter {
        if (valueFilter == null) {
            valueFilter = ValueFilter()
        }
        return valueFilter as ValueFilter
    }


    private inner class ValueFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            val results = Filter.FilterResults()
            var filterHash: DocksResult

            if (constraint != null && constraint.isNotEmpty()) {
                val filterList = java.util.ArrayList<DocksResult>()

                for (i in mFilterList.indices) {
                    if (mFilterList[i].place.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        filterHash = mFilterList[i]
                        filterList.add(filterHash)
                    }
                }
                results.count = filterList.size
                results.values = filterList
            } else {
                results.count = mFilterList.size
                results.values = mFilterList
            }
            return results
        }

        override fun publishResults(charSequence: CharSequence, results: Filter.FilterResults) {
            list = results.values as java.util.ArrayList<DocksResult>
            notifyDataSetChanged()
        }
    }

    interface LoadingAdapterCallbacks {
        fun OnLoadingClick(data: DocksResult)
        fun OnUnloadingClick(data: DocksResult)
    }
}