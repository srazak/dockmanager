package com.intutrack.myntradockmanager.ui

import android.app.NotificationManager
import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.material.snackbar.Snackbar
import com.intutrack.myntradockmanager.retrofit.helper.APIUtility
import com.intutrack.intutracksct.Retrofit.IntuApplication
import com.intutrack.myntradockmanager.preference.Preferences
import com.intutrack.myntradockmanager.R
import com.intutrack.myntradockmanager.utilities.Constants
import io.github.inflationx.viewpump.ViewPumpContextWrapper


open class BaseActivity : AppCompatActivity() {
    // variable to track event time
    var mLastClickTime: Long = 0
    internal lateinit var apiUtility: APIUtility
    internal var ad: AlertDialog.Builder? = null
    internal lateinit var dialog: AlertDialog
    internal var isActive = true
    internal var logoutReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (isActive) {
                ad = AlertDialog.Builder(this@BaseActivity)
                ad!!.setTitle(getString(R.string.app_name))
                ad!!.setMessage("Login Session Expired. Please Sign in Again.")
                ad!!.setCancelable(false)
                ad!!.setPositiveButton("ok") { dialog, which -> logout() }
                dialog = ad!!.create()
                dialog.setCancelable(false)
                if (!dialog.isShowing) {
                    dialog.show()
                }
            }
        }
    }
    fun isAllPermissionsGranted(grantResults: IntArray): Boolean {
        var isGranted = true
        for (grantResult in grantResults) {
            isGranted = grantResult == PackageManager.PERMISSION_GRANTED
            if (!isGranted)
                break
        }
        return isGranted
    }

    private var forceLogoutReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (isActive) {
                logout()
            }
        }
    }


    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContext = this@BaseActivity
        apiUtility = IntuApplication.apiUtility!!
        LocalBroadcastManager.getInstance(applicationContext)
            .registerReceiver(logoutReceiver, IntentFilter(Constants.ACTION_LOGOUT))
        LocalBroadcastManager.getInstance(applicationContext)
            .registerReceiver(forceLogoutReceiver, IntentFilter(Constants.ACTION_FORCE_LOGOUT))
    }

    override fun onPause() {
        super.onPause()
        isActive = false
    }

    override fun onResume() {
        super.onResume()
        isActive = true
    }

    protected fun showProgressDialog(message: String) {
        if (progressDialog != null) {
            progressDialog!!.dismiss()
        }
        progressDialog = ProgressDialog.show(this, getString(R.string.app_name), message)
    }

    protected fun showProgressDialog() {
        if (progressDialog != null) {
            progressDialog!!.dismiss()
        }
        progressDialog = ProgressDialog.show(
            this,
            getString(R.string.app_name),
            getString(R.string.prg_dial_pleaseWait)
        )
    }

    protected fun hideProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
            progressDialog = null
        }
    }

    //This function is cancel progress progressDialog
    fun cancelProgressDialog() {
        progressDialog!!.dismiss()
    }


    // Avoid Multiple click
    fun issingleClick(): Boolean {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
            return false
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        return true
    }

    fun hideKeyboard() {

        try {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            if (imm.isAcceptingText) {
                imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
            }
        } catch (e: Exception) {

        }

    }

    fun callAlertDialog(message: Int, context: Context) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message)
            .setTitle(resources.getString(R.string.app_name))
            .setCancelable(false)
        val alert = builder.create()
        alert.show()
    }

    fun callAlertDialog(message: String, context: Context) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message)
            .setTitle(resources.getString(R.string.app_name))
            .setCancelable(false)

        val alert = builder.create()
        alert.show()
    }

    fun showSnackBar(view: View, message: String) {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show()
    }

    fun ClearPrefs() {
        Preferences.removeAllPreference(applicationContext)
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    internal fun logout() {
        ClearPrefs()
        val notificationManager =
            applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
        val intent = Intent(this@BaseActivity, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    companion object {
        lateinit var mContext: Context
    }

}
