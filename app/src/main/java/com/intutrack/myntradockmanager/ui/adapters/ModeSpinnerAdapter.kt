package com.intutrack.myntradockmanager.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.intutrack.myntradockmanager.bean.login.Mode
import com.intutrack.myntradockmanager.R
import java.util.*


class ModeSpinnerAdapter(context: Context, data: ArrayList<Mode>) : BaseAdapter() {

    var list = ArrayList<Mode>()
    var mContext: Context
    private var inflater: LayoutInflater? = null

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(i: Int): Mode {
        return list[i]
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }


    override fun getView(i: Int, convertview: View?, viewGroup: ViewGroup): View {
        var view = convertview
        if (convertview == null) {
            view = inflater!!.inflate(R.layout.spinner_layout, viewGroup, false)
        }
        val label = view!!.findViewById<TextView>(R.id.spinnerTarget)
        label.text = list[i].modeData.key
        return view
    }

    fun getSelection(pos: Int): Mode {
        return list[pos]
    }

    init {
        list = data
        mContext = context
        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}