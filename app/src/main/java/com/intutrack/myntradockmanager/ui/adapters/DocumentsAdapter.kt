package com.intutrack.myntradockmanager.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.intutrack.gatekeeper.UI.Adapters.DocumentsChildAdapter
import com.intutrack.myntradockmanager.bean.openTransactions.Documents
import com.intutrack.myntradockmanager.R

class DocumentsAdapter(context: Context, data : ArrayList<Documents>) : RecyclerView.Adapter<DocumentsAdapter.DocumentsHolder>() {

    var list : ArrayList<Documents> = data
    var mContext : Context = context

    inner class DocumentsHolder (itemView : View) : RecyclerView.ViewHolder(itemView){

        var ll_docs : LinearLayout = itemView.findViewById(R.id.ll_rv)
        var rv_docs : RecyclerView = itemView.findViewById(R.id.rv_docs)
        var tv_category : TextView = itemView.findViewById(R.id.tv_category)
        var iv_expand : ImageView = itemView.findViewById(R.id.iv_expand)
        var rl_docs : RelativeLayout = itemView.findViewById(R.id.rl_transactions)

        init {
            rv_docs.layoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DocumentsHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_documents, parent, false)
        return DocumentsHolder(itemView)
    }

    override fun onBindViewHolder(holder: DocumentsHolder, position: Int) {
        val model = list[position]
        holder.tv_category.text = model.tag

        if (model.isExpanded){
            holder.ll_docs.visibility = View.VISIBLE
            holder.rv_docs.adapter = DocumentsChildAdapter(mContext,model.documentDetails)
        }else{
            holder.ll_docs.visibility = View.GONE
            holder.rv_docs.adapter = null
        }

        holder.rl_docs.setOnClickListener {
            expandToggle(position)
        }

    }

    fun expandToggle(position: Int) {
        list[position].isExpanded = !list[position].isExpanded
        notifyDataSetChanged()
    }

    fun setData(data : ArrayList<Documents>){
        /*val diffUtil = DocumentsDiffUtils(list,data)
        val diffResults = DiffUtil.calculateDiff(diffUtil)*/
        list = data
        notifyDataSetChanged()
        //diffResults.dispatchUpdatesTo(this)
    }

    fun getData() : ArrayList<Documents>{
        return list
    }

    override fun getItemCount(): Int {
     return list.size
    }

}