package com.intutrack.myntradockmanager.ui

import android.content.Intent
import android.os.Bundle
import com.bumptech.glide.Glide
import com.intutrack.myntradockmanager.bean.openTransactions.DocumentDetails
import com.intutrack.myntradockmanager.utilities.Constants.RESULT_IMAGE_UPDATE_COMPLETE
import com.intutrack.myntradockmanager.utilities.Constants.RESULT_REMARK_UPDATE_COMPLETE
import com.intutrack.myntradockmanager.databinding.ActivityImageBinding

class ImageActivity : BaseActivity() {

    private lateinit var binding: ActivityImageBinding
    lateinit var data: DocumentDetails
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityImageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        data = intent.getSerializableExtra("data") as DocumentDetails
        Glide.with(this).load(data.image).into(binding.ivImag)

        binding.appBar.title = data.name
        if (!data.remark.isNullOrEmpty()) binding.etRemark.setText(data.remark)

        binding.btnUpdate.setOnClickListener {
            if (!binding.etRemark.text.isNullOrEmpty()) {
                data.remark = binding.etRemark.text.toString()
                Intent().apply {
                    putExtra("data", data)
                    setResult(RESULT_REMARK_UPDATE_COMPLETE, intent)
                    finish()
                }
            } else {
                setResult(321)
                finish()
            }
        }

        binding.btnRetake.setOnClickListener {
            setResult(RESULT_IMAGE_UPDATE_COMPLETE)
            finish()
        }
    }

}