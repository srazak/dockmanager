package com.intutrack.gatekeeper.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.intutrack.myntradockmanager.bean.openTransactions.DocumentDetails
import com.intutrack.myntradockmanager.R

class DocumentsChildAdapter(context : Context,data : ArrayList<DocumentDetails>) : RecyclerView.Adapter<DocumentsChildAdapter.DocumentsChildHolder>() {
    var list: ArrayList<DocumentDetails> = data
    var mContext: Context = context
    var callback : DocumentsChildAdapterCallback = context as DocumentsChildAdapterCallback

    class DocumentsChildHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_label: CheckBox = itemView.findViewById(R.id.tv_label)
        var iv_action: ImageView = itemView.findViewById(R.id.iv_action)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DocumentsChildHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_docs_details, parent, false)
        return DocumentsChildHolder(itemView)
    }

    override fun onBindViewHolder(holder: DocumentsChildHolder, position: Int) {
        val model = list[position]
        holder.tv_label.text = model.name
        holder.tv_label.tag = position
        holder.tv_label.isChecked = model.isVerified
        if (model.image.isEmpty()) holder.iv_action.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_camera))
        else holder.iv_action.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_eye))
        holder.tv_label.setOnCheckedChangeListener { buttonView, isChecked ->
            val pos = holder.tv_label.tag as Int
            list[pos].isVerified = isChecked
        }

        holder.iv_action.setOnClickListener {
            callback.OnActionClick(model)
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface DocumentsChildAdapterCallback {
        fun OnActionClick(data : DocumentDetails)
    }

}