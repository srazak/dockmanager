package com.intutrack.myntradockmanager.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.intutrack.myntradockmanager.bean.docks.TripList
import com.intutrack.myntradockmanager.R
import com.intutrack.myntradockmanager.utilities.CommonUtils

class VehicleAdapter(val context: Context, val data: ArrayList<TripList>) :
    RecyclerView.Adapter<VehicleAdapter.MyViewHolder>() {

    var list = ArrayList<TripList>()

    init {
        list = data
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_time: TextView
        var tv_veh: TextView

        init {
            tv_time = itemView.findViewById(R.id.tv_time)
            tv_veh = itemView.findViewById(R.id.tv_veh)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_vehicle, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var model = list[position]
        if (model.trip_data != null) {
            holder.tv_veh.text = model.trip_data.vehicle
            holder.tv_time.text =
                CommonUtils.convertTimestampToDate(model.trip_data.gate_in_time_epoch)
        }

    }
}