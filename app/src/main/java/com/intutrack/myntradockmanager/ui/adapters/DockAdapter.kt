package com.intutrack.myntradockmanager.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.intutrack.myntradockmanager.bean.docks.DocksResult
import com.intutrack.myntradockmanager.R
import com.intutrack.myntradockmanager.utilities.CommonUtils

class DockAdapter(val context: Context, val data: ArrayList<DocksResult>) :
    RecyclerView.Adapter<DockAdapter.MyViewHolder>(), Filterable {

    var list: ArrayList<DocksResult> = ArrayList()
    var mFilterList: ArrayList<DocksResult> = ArrayList()
    var callback: DockAdapterCallbacks
    private var valueFilter: ValueFilter? = null

    init {
        list = data
        mFilterList = data
        callback = context as DockAdapterCallbacks
    }

    override fun getFilter(): Filter {
        if (valueFilter == null) {
            valueFilter = ValueFilter()
        }
        return valueFilter as ValueFilter
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var rv_vehicle: RecyclerView
        var tv_status: TextView
        var tv_dock_out: TextView
        var tv_dock: TextView
        var tv_no_0f_transactions: TextView
        var tv_veh: TextView
        var tv_time: TextView
        var ll_rv: LinearLayout
        var ll_details: LinearLayout
        var rl_transactions: RelativeLayout
        var v_line: View
        var iv_expand: ImageView


        init {
            rv_vehicle = itemView.findViewById(R.id.rv_vehicle)
            rv_vehicle.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            rv_vehicle.isNestedScrollingEnabled = false

            tv_status = itemView.findViewById(R.id.tv_status)
            tv_dock_out = itemView.findViewById(R.id.tv_dock_out)
            tv_dock = itemView.findViewById(R.id.tv_dock)
            tv_no_0f_transactions = itemView.findViewById(R.id.tv_no_of_transactions)
            tv_veh = itemView.findViewById(R.id.tv_veh)
            tv_time = itemView.findViewById(R.id.tv_time)
            ll_rv = itemView.findViewById(R.id.ll_rv)
            ll_details = itemView.findViewById(R.id.ll_details)
            rl_transactions = itemView.findViewById(R.id.rl_transactions)
            v_line = itemView.findViewById(R.id.v_line)
            iv_expand = itemView.findViewById(R.id.iv_expand)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_dock, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var model = list[position]

        holder.tv_dock.text = model.place

        holder.rl_transactions.setOnClickListener(View.OnClickListener {
            expandToggle(position)
        })


        if (model.isOccupied) {
            holder.v_line.visibility = View.VISIBLE
            holder.ll_details.visibility = View.VISIBLE

            holder.tv_veh.text = model.ongoing_trip_data.vehicle.toUpperCase()
            holder.tv_status.text = "Occupied"

            holder.tv_dock_out.text = "Dock Out"
            holder.tv_dock_out.setCompoundDrawablesWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_dock_out,
                0
            )
            holder.tv_time.text =
                CommonUtils.MillisecondsToTime(model.ongoing_trip_data.dock_time_epoch)

        } else {
            holder.v_line.visibility = View.GONE
            holder.ll_details.visibility = View.GONE

            holder.tv_veh.text = "N/A"
            holder.tv_status.text = "Empty"
            holder.tv_no_0f_transactions.text = "N/A"
            holder.tv_dock_out.text = "Dock In"
            holder.tv_dock_out.setCompoundDrawablesWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_dock_in,
                0
            )
        }

        if (model.tripList.size > 0) {
            holder.rl_transactions.visibility = View.VISIBLE
            holder.ll_rv.visibility = View.VISIBLE
            holder.tv_no_0f_transactions.text = model.tripList.size.toString() + " Transactions"
            holder.rv_vehicle.adapter = VehicleAdapter(context, model.tripList)
            if (model.isExpanded) {
                holder.ll_rv.visibility = View.VISIBLE
                holder.iv_expand.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_chevron_up
                    )
                )

            } else {
                holder.ll_rv.visibility = View.GONE
                holder.iv_expand.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_chevron_down
                    )
                )
            }
        } else {
            holder.rl_transactions.visibility = View.GONE
            holder.ll_rv.visibility = View.GONE
            holder.rv_vehicle.adapter = null
        }




        holder.tv_dock_out.setOnClickListener {
            if (model.isOccupied)
                callback.OnDockOut(model)
            else
                callback.OnDockIn(model)
        }
    }

    private inner class ValueFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            val results = Filter.FilterResults()
            var filterHash: DocksResult

            if (constraint != null && constraint.isNotEmpty()) {
                val filterList = java.util.ArrayList<DocksResult>()

                for (i in mFilterList.indices) {
                    if (mFilterList[i].place.lowercase()
                            .contains(constraint.toString().lowercase())
                    ) {
                        filterHash = mFilterList[i]
                        filterList.add(filterHash)
                    }
                }
                results.count = filterList.size
                results.values = filterList
            } else {
                results.count = mFilterList.size
                results.values = mFilterList
            }
            return results
        }

        override fun publishResults(charSequence: CharSequence, results: Filter.FilterResults) {
            list = results.values as java.util.ArrayList<DocksResult>
            notifyDataSetChanged()
        }
    }

    fun expandToggle(position: Int) {
        list[position].isExpanded = !list[position].isExpanded
        notifyDataSetChanged()
    }

    interface DockAdapterCallbacks {
        fun OnDockIn(data: DocksResult)
        fun OnDockOut(data: DocksResult)
    }

}