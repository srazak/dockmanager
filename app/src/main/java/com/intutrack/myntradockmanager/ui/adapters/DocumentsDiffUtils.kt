package com.intutrack.myntradockmanager.ui.adapters

import androidx.recyclerview.widget.DiffUtil
import com.intutrack.myntradockmanager.bean.openTransactions.Documents

class DocumentsDiffUtils(
    private val oldList : ArrayList<Documents>,
    private val newList : ArrayList<Documents>
) : DiffUtil.Callback() {


    override fun getOldListSize(): Int {
       return oldList.size
    }

    override fun getNewListSize(): Int {
       return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
       return oldList[oldItemPosition].tag == newList[newItemPosition].tag
               && oldList[oldItemPosition].documentDetails.size == newList[newItemPosition].documentDetails.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        if (oldList[oldItemPosition].documentDetails.size != newList[newItemPosition].documentDetails.size ||
            oldList[oldItemPosition].tag != newList[newItemPosition].tag){
                return false
        }else{
            val size = oldList[oldItemPosition].documentDetails.size
            for (index in 0 until size){
                if (oldList[oldItemPosition].documentDetails[index].isVerified != newList[newItemPosition].documentDetails[index].isVerified
                    || oldList[oldItemPosition].documentDetails[index].image != newList[newItemPosition].documentDetails[index].image)
                        return false
            }
            return true
        }
    }

}