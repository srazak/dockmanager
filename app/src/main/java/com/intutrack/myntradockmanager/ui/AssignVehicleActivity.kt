package com.intutrack.myntradockmanager.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.intutrack.myntradockmanager.retrofit.helper.APIUtility
import com.intutrack.myntradockmanager.bean.BaseResponse
import com.intutrack.myntradockmanager.bean.docks.TripList
import com.intutrack.myntradockmanager.R
import com.intutrack.myntradockmanager.ui.adapters.AssignVehicleAdapter
import com.intutrack.myntradockmanager.utilities.CommonUtils

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class AssignVehicleActivity : BaseActivity(), View.OnClickListener,
    AssignVehicleAdapter.AssignVehicleAdapterCallbacks,
    TextWatcher {
    lateinit var rv_vehicle: RecyclerView
    lateinit var et_search: EditText
    lateinit var tv_veh_no: TextView
    lateinit var btn_confirm: Button

    var data: ArrayList<TripList> = ArrayList()
    lateinit var dockId: String
    lateinit var tripId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_assign_vehicle)
        init()
    }


    fun init() {
        rv_vehicle = findViewById(R.id.rv_assign_veh)
        rv_vehicle.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        et_search = findViewById(R.id.et_search_dock)
        tv_veh_no = findViewById(R.id.tv_veh_no)

        btn_confirm = findViewById(R.id.btn_cofirm)
        btn_confirm.setOnClickListener(this)

        et_search.addTextChangedListener(this)

        if (intent.getSerializableExtra("data") != null) {
            data = intent.getSerializableExtra("data") as ArrayList<TripList>
            dockId = intent.getStringExtra("dockId")
        }

        rv_vehicle.adapter = AssignVehicleAdapter(this@AssignVehicleActivity, data)
    }

    override fun onClick(v: View?) {
        DockIn(tripId, dockId)
    }

    override fun OnSelected(data: TripList) {
        btn_confirm.visibility = View.VISIBLE

        tripId = data.tripId

    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        var adapter = rv_vehicle.adapter as AssignVehicleAdapter
        adapter.filter.filter(s)
    }

    fun DockIn(tripId: String, dockId: String) {
        apiUtility.DockIn(
            this,
            true,
            dockId,
            tripId,
            object : APIUtility.APIResponseListener<BaseResponse> {
                override fun onReceiveResponse(response: BaseResponse?) {
                    CommonUtils.showToast(applicationContext, response!!.message)
                    finish()
                }
            })
    }
}