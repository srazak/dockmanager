package com.intutrack.myntradockmanager.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.intutrack.myntradockmanager.bean.openTransactions.OpenTransactionResult
import com.intutrack.myntradockmanager.R

class OpenTransactionAdapter(val context: Context, val data: ArrayList<OpenTransactionResult>) :
    RecyclerView.Adapter<OpenTransactionAdapter.MyViewHolder>(), Filterable {

    var list: ArrayList<OpenTransactionResult> = ArrayList()
    var mFilterList: ArrayList<OpenTransactionResult> = ArrayList()
    var callback: OpenTransactionAdapterCallback
    private var valueFilter: ValueFilter? = null

    init {
        list = data
        mFilterList = data
        callback = context as OpenTransactionAdapterCallback
    }

    override fun getFilter(): Filter {
        if (valueFilter == null) {
            valueFilter = ValueFilter()
        }
        return valueFilter as ValueFilter
    }


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_veh_status: TextView
        var tv_trip_status: TextView
        var tv_assign_dock: TextView
        var tv_veh: TextView

        init {
            tv_assign_dock = itemView.findViewById(R.id.tv_assign_dock)
            tv_veh = itemView.findViewById(R.id.tv_veh_no)
            tv_trip_status = itemView.findViewById(R.id.tv_trip_status)
            tv_veh_status = itemView.findViewById(R.id.tv_veh_status)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_open_transaction, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var model = list[position]

        holder.tv_veh_status.text = model.vehicle_status
        holder.tv_trip_status.text = model.latestStatus
        holder.tv_veh.text = model.vehicle.toUpperCase()

        holder.tv_assign_dock.setOnClickListener {
            callback.AssignDock(model)
        }
    }

    private inner class ValueFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            val results = Filter.FilterResults()
            var filterHash: OpenTransactionResult

            if (constraint != null && constraint.length > 0) {
                val filterList = java.util.ArrayList<OpenTransactionResult>()

                for (i in mFilterList.indices) {
                    if (mFilterList[i].vehicle.toLowerCase()
                            .contains(constraint.toString().toLowerCase())
                    ) {
                        filterHash = mFilterList[i]
                        filterList.add(filterHash)
                    }
                }
                results.count = filterList.size
                results.values = filterList
            } else {
                results.count = mFilterList.size
                results.values = mFilterList
            }
            return results
        }

        override fun publishResults(charSequence: CharSequence, results: Filter.FilterResults) {
            list = results.values as java.util.ArrayList<OpenTransactionResult>
            notifyDataSetChanged()
        }
    }

    interface OpenTransactionAdapterCallback {
        fun AssignDock(data: OpenTransactionResult)
    }
}