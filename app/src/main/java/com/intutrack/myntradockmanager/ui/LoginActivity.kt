package com.intutrack.myntradockmanager.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.intutrack.myntradockmanager.retrofit.helper.APIUtility
import com.intutrack.intutracksct.Retrofit.IntuApplication
import com.intutrack.myntradockmanager.bean.login.LoginResponse
import com.intutrack.myntradockmanager.MainActivity
import com.intutrack.myntradockmanager.preference.PrefEntities
import com.intutrack.myntradockmanager.preference.Preferences
import com.intutrack.myntradockmanager.R
import com.intutrack.myntradockmanager.utilities.CommonUtils
import java.util.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var btn_login: Button
    lateinit var et_email: EditText
    lateinit var et_password: EditText
    lateinit var apiUtility: APIUtility

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        apiUtility = IntuApplication.apiUtility!!
        checkLogin()
        checkAndRequestPermissions()
        init()
    }

    fun init() {
        btn_login = findViewById(R.id.btn_login)
        btn_login.setOnClickListener(this)

        et_email = findViewById(R.id.et_user)
        et_password = findViewById(R.id.et_pass)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btn_login -> {
                validation()
            }
        }
    }

    private fun checkLogin() {
        if (Preferences.getPreference_boolean(applicationContext, PrefEntities.ISLOGIN)) {
            val intent = Intent(this@LoginActivity, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
    }


    private fun checkAndRequestPermissions() {
        val storagePermission = ContextCompat.checkSelfPermission(
            this@LoginActivity,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )
        val storageWritePermission = ContextCompat.checkSelfPermission(
            this@LoginActivity,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        val cameraPermission = ContextCompat.checkSelfPermission(
            this@LoginActivity,
            Manifest.permission.CAMERA
        )
        val callPhone = ContextCompat.checkSelfPermission(
            this@LoginActivity,
            Manifest.permission.CALL_PHONE
        )
        val listPermissionsNeeded = ArrayList<String>()

        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
        if (storageWritePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }
        if (callPhone != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CALL_PHONE)
        }

        if (listPermissionsNeeded.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                this@LoginActivity,
                listPermissionsNeeded.toTypedArray(), 1233
            )
        }
    }

    private fun validation() {
        et_email.error = null
        et_password.error = null

        val Email = et_email.text.toString().trim { it <= ' ' }

        var cancel = false
        var focusView: View? = null
        // Check for a valid email address.
        if (TextUtils.isEmpty(Email)) {
            if (Email.contains("@")) {
                if (!CommonUtils.isEmailValid(Email)) {
                    et_email.error = "Invalid Email Address"
                    focusView = et_email
                    cancel = true
                }
            } else {
                if (!CommonUtils.isValidMobNumber(Email)) {
                    et_email.error = "Invalid Login"
                    focusView = et_email
                    cancel = true
                }
            }

        } else if (TextUtils.isEmpty(et_password.text.toString())) {
            et_password.error = "Enter a Password"
            focusView = et_password
            cancel = true
        }

        if (cancel) {
            focusView!!.requestFocus()

        } else {
            Login(Email, et_password.text.toString())
        }
    }


    private fun Login(email: String, password: String) {
        apiUtility.Login(
            this,
            true,
            email,
            password,
            object : APIUtility.APIResponseListener<LoginResponse> {
                override fun onReceiveResponse(response: LoginResponse?) {
                    Preferences.setPreference(applicationContext, PrefEntities.ISLOGIN, true)
                    Preferences.setPreference(
                        applicationContext,
                        PrefEntities.WAREHOUSE,
                        response!!.result[0].warehouse_config[0].name
                    )
                    Preferences.setPreference(
                        applicationContext,
                        PrefEntities.EMAIL,
                        response.result[0].username
                    )

                    Preferences.storeWarehouse(
                        applicationContext,
                        response.result[0].warehouse_config[0].dest_warehouse,
                        PrefEntities.DESTINATIONS
                    )
                    Preferences.storeKeyValue(
                        applicationContext,
                        response.result[0].warehouse_config[0].placement_type,
                        PrefEntities.PLACEMENT_TYPE
                    )
                    Preferences.storeKeyValue(
                        applicationContext,
                        response.result[0].warehouse_config[0].vehicle_status,
                        PrefEntities.VEHICLE_STATUS
                    )
                    Preferences.storeKeyValue(
                        applicationContext,
                        response.result[0].warehouse_config[0].vendors,
                        PrefEntities.VENDORS
                    )
                    Preferences.storeKeyValue(
                        applicationContext,
                        response.result[0].warehouse_config[0].purpose,
                        PrefEntities.PURPOSE
                    )
                    Preferences.storeKeyValue(
                        applicationContext,
                        response.result[0].warehouse_config[0].truck_type,
                        PrefEntities.TRUCK_TYPE
                    )
                    Preferences.storeClientType(
                        applicationContext,
                        response.result[0].warehouse_config[0].client_trip_type,
                        PrefEntities.CLIENT_TYPE
                    )


                    val intent = Intent(this@LoginActivity, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                }
            })
    }
}